package eclair.course.cst.androideclair.request_methods.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import eclair.course.cst.androideclair.helpers.Constants;
import eclair.course.cst.androideclair.interfaces.OnGetMoviesAsJSONListener;
import eclair.course.cst.androideclair.models.Movie;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.GET;
import retrofit2.http.Query;


import static eclair.course.cst.androideclair.helpers.Constants.BASE_URL;

/**
 * Created by Gabriel on 3/6/2018.
 */

public class GetMoviesByRetrofit implements Callback<GetMoviesByRetrofit.MoviesList> {

    private static final String TAG = "GetMoviesByRetrofit";

    private OnGetMoviesAsJSONListener mListener;

    public GetMoviesByRetrofit(OnGetMoviesAsJSONListener delegate) {
        this.mListener = delegate;
    }

    public interface MoviesApiDB {
        @GET("3/movie/popular")
        Call<MoviesList> onGetMoviesList(@Query(Constants.ARG_API_KEY) String api_key);
    }

    public class MoviesList{
        public ArrayList<Movie> getResults() {
            return results;
        }

        public void setResults(ArrayList<Movie> results) {
            this.results = results;
        }

        private ArrayList<Movie> results;
    }

    public void execute(String apiKey) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        MoviesApiDB moviesApiDB = retrofit.create(MoviesApiDB.class);

        Call<MoviesList> call = moviesApiDB.onGetMoviesList(apiKey);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<MoviesList> call, Response<MoviesList> response) {
        Log.e(GetMoviesByRetrofit.class.getSimpleName(), "request by retrofit done");

        mListener.onGetMoviesSuccess(response.body().getResults());
    }

    @Override
    public void onFailure(Call<MoviesList> call, Throwable t) {
        mListener.onGetMoviesFailed();
    }

}