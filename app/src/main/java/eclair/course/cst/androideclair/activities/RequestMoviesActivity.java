package eclair.course.cst.androideclair.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import eclair.course.cst.androideclair.R;
import eclair.course.cst.androideclair.request_methods.OnRequestMovies.OnGetMoviesListener;
import eclair.course.cst.androideclair.models.Movie;
import eclair.course.cst.androideclair.request_methods.OnRequestMovies;

public class RequestMoviesActivity extends AppCompatActivity implements View.OnClickListener{

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_movies);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Loading...");

        TextView tv_request_movies_volley = findViewById(R.id.tv_request_movies_volley);
        tv_request_movies_volley.setOnClickListener(this);

        TextView tv_request_movies_httpget = findViewById(R.id.tv_request_movies_httpget);
        tv_request_movies_httpget.setOnClickListener(this);

        TextView tv_request_movies_retrofit = findViewById(R.id.tv_request_movies_retrofit);
        tv_request_movies_retrofit.setOnClickListener(this);

    }

    private OnGetMoviesListener getMoviesListener() {
        return new OnGetMoviesListener() {
            @Override
            public void onGetMoviesSuccess(ArrayList<Movie> mMoviesList) {
                mProgressDialog.dismiss();

                if(mMoviesList == null){
                    return;
                }

                String requestFinishedText = "Request finished! "
                        + mMoviesList.size()
                        + " movies has been found!";
                Toast.makeText(RequestMoviesActivity.this, requestFinishedText, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onGetMoviesFailed() {
                mProgressDialog.dismiss();
                Toast.makeText(RequestMoviesActivity.this, "Request failed!", Toast.LENGTH_SHORT)
                        .show();
            }
        };
    }

    private void onRequestMovies(int method){
        mProgressDialog.show();
        new OnRequestMovies(RequestMoviesActivity.this, getMoviesListener())
                .execute(method);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.tv_request_movies_volley:
                onRequestMovies(OnRequestMovies.REQUEST_TYPE_VOLLEY);
                break;

            case R.id.tv_request_movies_httpget:
                onRequestMovies(OnRequestMovies.REQUEST_TYPE_HTTP);
                break;

            case R.id.tv_request_movies_retrofit:
                onRequestMovies(OnRequestMovies.REQUEST_TYPE_RETROFIT);
                break;
        }
    }
}