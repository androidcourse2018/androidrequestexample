package eclair.course.cst.androideclair.request_methods.volley;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import eclair.course.cst.androideclair.ApplicationController;
import eclair.course.cst.androideclair.helpers.Constants;
import eclair.course.cst.androideclair.interfaces.OnGetMoviesAsJSONListener;
import eclair.course.cst.androideclair.models.Movie;

/**
 * Created by Gabriel on 2/28/2018.
 */

public class GetMoviesByVolley implements Response.Listener<JSONObject> {

    private static final String TAG = "GetMoviesByVolley";

    private RequestQueue mRequestQueue;
    private OnGetMoviesAsJSONListener mListener;

    private ApplicationController myApp;

    public GetMoviesByVolley(OnGetMoviesAsJSONListener delegate) {

        this.mListener = delegate;
        this.myApp = ApplicationController.getInstance();
    }

    public void execute(String url) {
        mRequestQueue = ApplicationController.getInstance().getRequestQueue();
        CustomJSONObjectRequest request = new CustomJSONObjectRequest(
                Request.Method.GET,
                url,
                null,
                this,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Failed to make init request!");
                        mListener.onGetMoviesFailed();
                    }
                });

        request.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(request);
    }

    @Override
    public void onResponse(final JSONObject response) {
        Log.e(TAG, "volley response done");

        mListener.onGetMoviesSuccess(response);
    }



}
