package eclair.course.cst.androideclair.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import eclair.course.cst.androideclair.R;

public class MainEclair extends AppCompatActivity implements View.OnClickListener{

    private TextView tv_start_course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_eclair);

        setUpViews();
    }

    private void setUpViews(){
        tv_start_course = findViewById(R.id.tv_start_course);
        tv_start_course.setOnClickListener(this);
    }

    private void goToCourse(){
        Intent intent = new Intent(this, RequestMoviesActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.tv_start_course:
                goToCourse();
                break;

        }

    }
}
