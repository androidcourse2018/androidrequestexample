package eclair.course.cst.androideclair.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Gabriel on 2/28/2018.
 */

public class Movie {

    private final String ARG_TITLE = "title";
    private final String ARG_OVERVIEW = "overview";

    private String title;
    private String overview;

    public Movie(){

    }

    public Movie(String title, String overview){
        this.title = title;
        this.overview = overview;
    }

    public void onParseMovie(JSONObject movieJSON) throws JSONException {

        this.title = movieJSON.getString(ARG_TITLE);
        this.overview = movieJSON.getString(ARG_OVERVIEW);

    }

    /*Getters & Setters*/

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

}
