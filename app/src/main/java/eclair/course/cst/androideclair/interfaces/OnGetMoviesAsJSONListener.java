package eclair.course.cst.androideclair.interfaces;

import org.json.JSONObject;

import java.util.ArrayList;

import eclair.course.cst.androideclair.models.Movie;

/**
 * Created by Gabriel on 3/6/2018.
 */

public interface OnGetMoviesAsJSONListener {
    void onGetMoviesSuccess(JSONObject mMoviesList);
    void onGetMoviesSuccess(ArrayList<Movie> mMoviesList);
    void onGetMoviesFailed();
}
