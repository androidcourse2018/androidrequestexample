package eclair.course.cst.androideclair.request_methods;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import eclair.course.cst.androideclair.R;
import eclair.course.cst.androideclair.helpers.Constants;
import eclair.course.cst.androideclair.interfaces.OnGetMoviesAsJSONListener;
import eclair.course.cst.androideclair.models.Movie;
import eclair.course.cst.androideclair.request_methods.http.GetMoviesByHttpGet;
import eclair.course.cst.androideclair.request_methods.retrofit.GetMoviesByRetrofit;
import eclair.course.cst.androideclair.request_methods.volley.GetMoviesByVolley;

/**
 * Created by Gabriel on 2/28/2018.
 */

public class OnRequestMovies implements OnGetMoviesAsJSONListener {

    public interface OnGetMoviesListener {
        void onGetMoviesSuccess(ArrayList<Movie> mMoviesList);
        void onGetMoviesFailed();
    }

    public final static int REQUEST_TYPE_HTTP = 0;
    public final static int REQUEST_TYPE_VOLLEY = 1;
    public final static int REQUEST_TYPE_RETROFIT = 2;

    private OnGetMoviesListener mActivityListener;

    private String GET_MOVIES_URL = "";

    private ArrayList<Movie> mMoviesList;

    private Context mContext;

    public OnRequestMovies(Context mContext, OnGetMoviesListener mActivityListener){
        this.mActivityListener = mActivityListener;
        this.mContext = mContext;
        this.mMoviesList = new ArrayList<>();

        GET_MOVIES_URL = Constants.BASE_URL
                + mContext.getString(R.string.get_popular_movies)
                + "?" + Constants.ARG_API_KEY
                + "=" + mContext.getString(R.string.tmdb_api_key);
    }

    public void execute(int requestType){
        switch (requestType){

            case REQUEST_TYPE_HTTP:
                requestByHttp();
                break;

            case REQUEST_TYPE_VOLLEY:
                requestByVolley();
                break;

            case REQUEST_TYPE_RETROFIT:
                requestByRetrofit();
                break;
        }
    }

    private void requestByHttp(){
        new GetMoviesByHttpGet(this).execute(GET_MOVIES_URL);
    }

    private void requestByVolley(){
        new GetMoviesByVolley(this).execute(GET_MOVIES_URL);
    }

    private void requestByRetrofit(){
        new GetMoviesByRetrofit(this).execute(mContext.getString(R.string.tmdb_api_key));
    }

    /*OnGetMoviesAsJSONListener methods*/
    @Override
    public void onGetMoviesSuccess(JSONObject response) {
        try {
            parseMoviesList(response);
            mActivityListener.onGetMoviesSuccess(mMoviesList);
        } catch (JSONException ex){
            Log.e(OnRequestMovies.class.getSimpleName(), ex.getMessage());
            mActivityListener.onGetMoviesFailed();
        }
    }

    @Override
    public void onGetMoviesSuccess(ArrayList<Movie> mMoviesList) {
        mActivityListener.onGetMoviesSuccess(mMoviesList);
    }

    @Override
    public void onGetMoviesFailed() {
        this.mActivityListener.onGetMoviesFailed();
    }

    //Parse movies JSON
    private void parseMoviesList(JSONObject response) throws JSONException{
        JSONArray results = response.getJSONArray(Constants.ARG_RESULTS);

        for (int i = 0; i < results.length(); i++) {
            try {
                JSONObject movieJSON = results.getJSONObject(i);

                Movie mMovie = new Movie();
                mMovie.onParseMovie(movieJSON);

                this.mMoviesList.add(mMovie);
            } catch (JSONException ex) {
                Log.e(OnRequestMovies.class.getSimpleName(), "Broken movie: " + ex.getMessage());
            }
        }
    }
}
