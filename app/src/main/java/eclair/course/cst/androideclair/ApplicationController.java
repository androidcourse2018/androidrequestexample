package eclair.course.cst.androideclair;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 * Created by Gabriel on 2/28/2018.
 */

public class ApplicationController extends Application {

    private static final int SIZE_MAX_CACHE = 10 * 1024 * 1024;

    private static ApplicationController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    /**
     * Get a singleton instance of ApplicationController.
     */
    public static synchronized ApplicationController getInstance() {
        return mInstance;
    }

    /**
     * Get a singleton instance of {@link RequestQueue}.
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Get a singleton instance of {@link ImageLoader}.
     * TODO optimize caching
     */
    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(getRequestQueue(), new ImageLoader.ImageCache() {

                private final LruCache<String, Bitmap> cache = new LruCache<String, Bitmap>(SIZE_MAX_CACHE) {
                    @Override
                    protected int sizeOf(String key, Bitmap value) {
                        return value.getByteCount();
                    }
                };

                @Override
                public Bitmap getBitmap(String url) {
                    return cache.get(url);
                }

                @Override
                public void putBitmap(String url, Bitmap bitmap) {
                    cache.put(url, bitmap);
                }
            });
        }

        return mImageLoader;
    }

}
