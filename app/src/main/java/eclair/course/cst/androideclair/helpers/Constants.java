package eclair.course.cst.androideclair.helpers;

/**
 * Created by Gabriel on 2/28/2018.
 */

public class Constants {

    public static final String BASE_URL = "https://api.themoviedb.org/";

    public static final String ARG_RESULTS = "results";

    public final static String ARG_API_KEY = "api_key";

}
